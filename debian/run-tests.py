import os
import sys

from distutils.command.build import build
from distutils.core import Distribution
build = build(Distribution())
build.finalize_options()
sys.path[0:0] = [os.path.abspath(build.build_platlib)]

import gamera.paths
assert os.path.splitext(gamera.paths.__file__)[0] == os.path.join(
    sys.path[0], 'gamera', 'paths'), 'Wrong import path: ' + gamera.paths.__file__

os.chdir(os.path.join(os.path.dirname(__file__), '..', 'tests'))
gamera.paths.test = os.path.join('..', 'gamera', 'test')
sys.argv[1:1] = ['--verbose']

execfile('/usr/bin/nosetests')
